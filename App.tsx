import React, {useEffect, useState} from 'react';
import {Appearance, View} from 'react-native';
import NetInfo, {NetInfoState} from '@react-native-community/netinfo';
import AddExpense from './src/Dialogs/AddExpense';
import {Button, Provider} from 'react-native-paper';
import Login from './src/Components/Login';
import {getToken, removeUserSession} from './src/LocalStorage/Auth';
import BudgetComponent from './src/Components/BudgetComponent';
import Budget from './src/Interfaces/Budget.interface';
import moment from 'moment';
import StartEndTime from './src/Interfaces/StartEndTime.interface';
import {getBudgetLocal} from './src/LocalStorage/budgetLocal';
import Category from './src/Interfaces/Category.interface';
import {getExpenseCategoryLocal} from './src/LocalStorage/categoryLocal';
import Currency from './src/Interfaces/Currency.interface';
import {getCurrencyLocal} from './src/LocalStorage/currencyLocal';
import Expense from './src/Interfaces/Expense.interface';
import {expenseApi} from './src/Api/expenseApi';
import AppBar from './src/Components/AppBar';
import {handleLoadAllDataOnline} from './src/Helpers/HandleLoadAllDataOnline';
import Task from './src/Interfaces/Task.interface';
import {getTaskLocal} from './src/LocalStorage/taskLocal';
import {
  getTempExpenseLocal,
  removeTempExpenseLocal,
} from './src/LocalStorage/tempExpensesLocal';

const App = () => {
  const [osTheme, setOsTheme] = useState<string>('light');
  const [isSelectTransactionPeriod, setIsSelectTransactionPeriod] =
    useState<boolean>(false);
  const [selectedTransactionPeriod, setSelectedTransactionPeriod] =
    useState<string>('');
  const [isAddExpenseOpened, setIsAddExpenseOpened] = useState<boolean>(false);
  const [isUserLogedIn, setIsUserLogedIn] = useState<boolean>(false);
  const [budget, setBudget] = useState<Budget>({} as Budget);
  const [expenseCategories, setExpenseCategories] = useState<Category[]>([]);
  const [currencies, setCurrencies] = React.useState<Currency[]>([]);
  const [budgetDateTime, setBudgetDateTime] = React.useState<StartEndTime>({
    startDateTime: new Date(moment().startOf('month').toString()),
    endDateTime: new Date(),
  });
  const [tasks, setTasks] = useState<Task[]>([]);

  const loadAllDataOffline = async () => {
    getBudgetLocal().then(res => setBudget(res));
    getExpenseCategoryLocal().then(res => setExpenseCategories(res));
    getCurrencyLocal().then(res => setCurrencies(res));
    getTaskLocal().then(res => setTasks(res));
  };

  const sendStoredData = async () => {
    let expenses: Expense[] = await getTempExpenseLocal();
    if (expenses.length > 0) {
      await expenseApi
        .addExpenses(expenses, await getToken())
        .then(res => removeTempExpenseLocal())
        .catch(err => console.log(err));
    }
  };

  useEffect(() => {
    const userTheme = Appearance.getColorScheme();
    if (typeof userTheme === 'string') {
      setOsTheme(userTheme);
    }
    //Load all required Data on app load.
    loadAllDataOffline();
    NetInfo.fetch().then((isConnected: NetInfoState) => {
      if (isConnected.isConnected) {
        handleLoadAllDataOnline(
          budgetDateTime,
          setBudget,
          setExpenseCategories,
          setCurrencies,
          setTasks,
        );
        sendStoredData();
      }
    });
  }, []);

  const handleLogout = () => {
    removeUserSession();
    setIsUserLogedIn(false);
  };

  return (
    <Provider>
      <AppBar />
      <View style={{position: 'absolute', top: 75}}>
        {isUserLogedIn ? (
          <BudgetComponent
            setIsAddExpenseOpened={setIsAddExpenseOpened}
            budget={budget}
            setIsSelectTransactionPeriod={setIsSelectTransactionPeriod}
            selectedTransactionPeriod={selectedTransactionPeriod}
            tasks={tasks}
            osTheme={osTheme}
          />
        ) : (
          <Login setIsUserLogedIn={setIsUserLogedIn} />
        )}
        <AddExpense
          isOpened={isAddExpenseOpened}
          setIsOpened={setIsAddExpenseOpened}
          expenseCategories={expenseCategories}
          currencies={currencies}
        />
        {isUserLogedIn && <Button onPress={handleLogout}>Logout</Button>}
      </View>
    </Provider>
  );
};

export default App;
