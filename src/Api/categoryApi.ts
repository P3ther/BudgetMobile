import Category from '../Interfaces/Category.interface';
import {axiosInstance} from './axiosInstance';

export enum categoryEndpoints {
  categories = 'categories/',
  expenseCategories = 'categories/expense/',
}

export const categoryApi = {
  getCategories: (token: string) =>
    axiosInstance
      .get<Category[]>(categoryEndpoints.categories, {
        headers: {
          Authorization: token,
        },
      })
      .then(res => res.data),
  getExpenseCategories: (token: string) =>
    axiosInstance
      .get<Category[]>(categoryEndpoints.expenseCategories, {
        headers: {
          Authorization: token,
        },
      })
      .then(res => res.data),
};
