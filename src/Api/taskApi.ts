import Task from '../Interfaces/Task.interface';
import {axiosInstance} from './axiosInstance';

enum endPoints {
  task = '/task/',
  tasks = '/tasks/',
}

export const taskApi = {
  getTask: (taskId: number, token: string) =>
    axiosInstance
      .get<Task>(endPoints.task + taskId + '/', {
        headers: {
          Authorization: token,
        },
      })
      .then(res => res.data),
  createTask: (newTask: Task, token: string) =>
    axiosInstance
      .post<Task>(endPoints.task, newTask, {
        headers: {
          Authorization: token,
        },
      })
      .then(res => res.data),
  updateTask: (editedTask: Task, token: string) =>
    axiosInstance
      .put<Task>(endPoints.task, editedTask, {
        headers: {
          Authorization: token,
        },
      })
      .then(res => res.data),
  listTasks: (token: string) =>
    axiosInstance
      .get<Task[]>(endPoints.tasks, {
        headers: {
          Authorization: token,
        },
      })
      .then(res => res.data),
  removeTask: (taskId: number, token: string) =>
    axiosInstance.delete(endPoints.task + taskId + '/', {
      headers: {
        Authorization: token,
      },
    }),
};
