import Expense from '../Interfaces/Expense.interface';
import StartEndTime from '../Interfaces/StartEndTime.interface';
import {axiosInstance} from './axiosInstance';

enum endPoints {
  expense = 'expense/',
  expenses = 'expenses/',
  expensesByTime = 'expense/time/',
}

export const expenseApi = {
  addExpense: (expense: Expense, token: string) =>
    axiosInstance
      .post(endPoints.expense, expense, {
        headers: {
          Authorization: token,
        },
      })
      .then(res => res.data),
  addExpenses: (expenses: Expense[], token: string) =>
    axiosInstance
      .post<Expense[]>(endPoints.expenses, expenses, {
        headers: {
          Authorization: token,
        },
      })
      .then(res => res.data),
  getExpensesByTime: (dateRange: StartEndTime, token: string) =>
    axiosInstance
      .post<Expense[]>(endPoints.expensesByTime, dateRange, {
        headers: {
          Authorization: token,
        },
      })
      .then(res => res.data),
};