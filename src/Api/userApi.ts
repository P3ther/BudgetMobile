import User from '../Interfaces/User.interface';
import {axiosInstance} from './axiosInstance';

enum userEndpoints {
  login = 'login',
}

export const userApi = {
  login: (user: User) =>
    axiosInstance.post(userEndpoints.login, user).then(res => res),
};
