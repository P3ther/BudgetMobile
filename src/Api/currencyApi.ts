import Currency from '../Interfaces/Currency.interface';
import {axiosInstance} from './axiosInstance';

enum currencyEndpoints {
  currency = 'currency/',
  editCurrency = 'currency/edit/',
}

export const currencyApi = {
  getCurrencies: (token: string) =>
    axiosInstance
      .get<Currency[]>(currencyEndpoints.currency, {
        headers: {
          Authorization: token,
        },
      })
      .then(res => res.data),
};
