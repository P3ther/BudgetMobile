import Budget from '../Interfaces/Budget.interface';
import StartEndTime from '../Interfaces/StartEndTime.interface';
import {axiosInstance} from './axiosInstance';

enum budgetEndpoints {
  budget = 'budget/',
}

export const budgetApi = {
  getTimedBudget: (startEndTime: StartEndTime, token: string) =>
    axiosInstance
      .post<Budget>(budgetEndpoints.budget, startEndTime, {
        headers: {
          Authorization: token,
        },
      })
      .then(res => res.data),
};
