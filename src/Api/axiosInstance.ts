import axios from 'axios';
import {getToken} from '../LocalStorage/Auth';
import {backEndAddress} from './apiVariables';

export const axiosInstance = axios.create({
  baseURL: backEndAddress,
  headers: {Authorization: getToken()},
});
