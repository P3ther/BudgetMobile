import {Dispatch, SetStateAction} from 'react';

import {getToken} from '../LocalStorage/Auth';
import {setBudgetLocal} from '../LocalStorage/budgetLocal';
import {setExpenseCategoryLocal} from '../LocalStorage/categoryLocal';
import {setCurrencyLocal} from '../LocalStorage/currencyLocal';
import {setTaskLocal} from '../LocalStorage/taskLocal';
import {budgetApi} from '../Api/budgetApi';
import {categoryApi} from '../Api/categoryApi';
import {currencyApi} from '../Api/currencyApi';
import {taskApi} from '../Api/taskApi';

import Budget from '../Interfaces/Budget.interface';
import StartEndTime from '../Interfaces/StartEndTime.interface';
import Category from '../Interfaces/Category.interface';
import Currency from '../Interfaces/Currency.interface';
import Task from '../Interfaces/Task.interface';
import {expenseApi} from '../Api/expenseApi';
import moment from 'moment';
import Expense from '../Interfaces/Expense.interface';
import {setExpensesLocal} from '../LocalStorage/expenseLocal';

export const handleLoadAllDataOnline = async (
  budgetDateTime: StartEndTime,
  setBudget: Dispatch<SetStateAction<Budget>>,
  setExpenses: Dispatch<SetStateAction<Expense[]>>,
  setExpenseCategories: Dispatch<SetStateAction<Category[]>>,
  setCurrencies: Dispatch<SetStateAction<Currency[]>>,
  setTasks: Dispatch<SetStateAction<Task[]>>,
) => {
  budgetApi.getTimedBudget(budgetDateTime, await getToken()).then(res => {
    setBudget(res);
    setBudgetLocal(res);
  });
  const expenseRange: StartEndTime = {
    startDateTime: new Date(moment().startOf('month').toString()),
    endDateTime: new Date(moment().endOf('month').toString()),
  };
  expenseApi.getExpensesByTime(expenseRange, await getToken()).then(res => {
    setExpensesLocal(res);
    setExpenses(res);
  });

  categoryApi.getExpenseCategories(await getToken()).then(res => {
    setExpenseCategories(res);
    setExpenseCategoryLocal(res);
  });
  currencyApi.getCurrencies(await getToken()).then(res => {
    setCurrencies(res);
    setCurrencyLocal(res);
  });
  taskApi.listTasks(await getToken()).then(res => {
    setTasks(res);
    setTaskLocal(res);
  });
};
