import AsyncStorage from '@react-native-async-storage/async-storage';
import React, {
  Dispatch,
  Fragment,
  SetStateAction,
  useEffect,
  useState,
} from 'react';
import {Controller, useForm} from 'react-hook-form';
import {Dimensions} from 'react-native';
import {Button, Paragraph, TextInput} from 'react-native-paper';
import {userApi} from '../Api/userApi';
import User from '../Interfaces/User.interface';
import {isAuthenticated, setUserSession} from '../LocalStorage/Auth';

interface Props {
  setIsUserLogedIn: Dispatch<SetStateAction<boolean>>;
}
const Login = ({setIsUserLogedIn}: Props) => {
  const [isLogedIn, setIsLogedIn] = useState<boolean>(false);
  const [hasError, setHasError] = useState<boolean>(false);
  const [hidePassword, setHidePassword] = useState<boolean>(true);
  const {
    handleSubmit,
    control,
    formState: {isDirty, isValid},
  } = useForm<User>({
    //mode: "onChange",
    //resolver: yupResolver(ExpenseValidation),
  });
  const onSubmit = (data: User) => {
    const loginUser: User = {
      username: data['username'],
      password: data['password'],
    };
    userApi
      .login(loginUser)
      .then(res => {
        setUserSession(res.headers.authorization, loginUser.username);
        setIsLogedIn(!isLogedIn);
      })
      .catch(err => {
        setHasError(true);
        console.log(err);
      });
  };

  useEffect(() => {
    const handleAuth = async () => {
      setIsUserLogedIn(await isAuthenticated());
    };

    handleAuth();
  }, [isLogedIn]);

  return (
    <Fragment>
      <Paragraph>Username:</Paragraph>
      <Controller
        control={control}
        name="username"
        render={({field: {onChange, value, onBlur, ref}}) => (
          <TextInput
            dense
            value={value}
            onChange={value => {
              setHasError(false);
              onChange(value.nativeEvent.text);
            }}
            ref={ref}
            error={hasError}
            style={{width: Dimensions.get('window').width}}
          />
        )}
      />
      <Paragraph>Password:</Paragraph>
      <Controller
        control={control}
        name="password"
        render={({field: {onChange, value, onBlur, ref}}) => (
          <TextInput
            dense
            value={value}
            onChange={value => {
              setHasError(false);
              onChange(value.nativeEvent.text);
            }}
            ref={ref}
            secureTextEntry={hidePassword}
            error={hasError}
            style={{width: Dimensions.get('window').width}}
            right={
              <TextInput.Icon
                name="eye"
                onPress={() => setHidePassword(!hidePassword)}
              />
            }
          />
        )}
      />
      <Button mode="outlined" onPress={handleSubmit(onSubmit)}>
        Login
      </Button>
    </Fragment>
  );
};

export default Login;
