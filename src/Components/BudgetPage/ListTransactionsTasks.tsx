import React, {
  Dispatch,
  Fragment,
  SetStateAction,
  useEffect,
  useMemo,
  useState,
} from 'react';
import {Dimensions, ScrollView, StyleSheet, Text, View} from 'react-native';
import {Button, ListItem} from 'react-native-elements';
import {BudgetListEnum} from '../../Enums/BudgetListEnum';
import Expense from '../../Interfaces/Expense.interface';
import Task from '../../Interfaces/Task.interface';
import {getExpensesLocal} from '../../LocalStorage/expenseLocal';
import {
  getTempExpenseLocal,
  setTempExpenseLocal,
} from '../../LocalStorage/tempExpensesLocal';
import {
  systemDarkThemeBackground,
  systemDarkThemeFont,
} from '../../Styles/SystemVariables';

interface Props {
  isAddExpenseOpened: boolean;
  selectedList: BudgetListEnum;
  tasks: Task[];
  osTheme: string;
  expenses: Expense[];
  setExpenses: Dispatch<SetStateAction<Expense[]>>;
}

const styles = StyleSheet.create({
  listItem: {padding: 0},
  listContainerDarkMode: {backgroundColor: systemDarkThemeBackground},
  listContainerLightMode: {backgroundColor: systemDarkThemeBackground},
  listTextDarkMode: {color: systemDarkThemeFont},
  container: {
    height: 440,
    maxHeight: 440,
  },
});

const ListTransactionsTasks = ({
  isAddExpenseOpened,
  selectedList,
  tasks,
  osTheme,
  expenses,
  setExpenses,
}: Props) => {
  const [tempExpenses, setTempExpenses] = useState<Expense[]>([]);

  useEffect(() => {
    getTempExpenseLocal().then(res => {
      res.sort(
        (a: Expense, b: Expense) =>
          new Date(b.date).getTime() - new Date(a.date).getTime(),
      );
      setTempExpenses(res);
    });
    getExpensesLocal().then(res => {
      res.sort(
        (a: Expense, b: Expense) =>
          new Date(b.date).getTime() - new Date(a.date).getTime(),
      );
      setExpenses(res);
    });
  }, [selectedList, isAddExpenseOpened]);

  return (
    <Fragment>
      {selectedList === BudgetListEnum.tasks && (
        <Fragment>
          <View>
            <Button
              type="clear"
              //onPress={() => setIsAddExpenseOpened(true)}
              title="Create Task"
              titleStyle={{color: '#43aa8b'}}
              buttonStyle={{
                width: Dimensions.get('window').width,
                borderColor: '#43aa8b',
                alignSelf: 'stretch',
              }}
            />
          </View>
          <ScrollView style={styles.container}>
            {tasks.map((task: Task, key: number) => (
              <ListItem
                key={key}
                containerStyle={
                  osTheme !== 'light' && styles.listContainerDarkMode
                }
                style={styles.listItem}>
                <Text style={osTheme !== 'light' && styles.listTextDarkMode}>
                  {task.taskName}
                </Text>
              </ListItem>
            ))}
          </ScrollView>
        </Fragment>
      )}
      {selectedList === BudgetListEnum.transactions && (
        <ScrollView style={styles.container}>
          {tempExpenses.length > 0 && <Text>Not Synced Tasks</Text>}
          {tempExpenses.map((expense: Expense, key: number) => (
            <Fragment>
              <ListItem
                key={key}
                containerStyle={
                  osTheme !== 'light' && styles.listContainerDarkMode
                }
                style={styles.listItem}>
                <ListItem.Content>
                  <ListItem.Title
                    style={osTheme !== 'light' && styles.listTextDarkMode}>
                    {expense.name}
                  </ListItem.Title>
                  <ListItem.Subtitle>
                    {'Value: ' + expense.price + ', from date: ' + expense.date}
                  </ListItem.Subtitle>
                </ListItem.Content>
              </ListItem>
            </Fragment>
          ))}
          {expenses.length > 0 && <Text>Synced Tasks</Text>}
          {expenses.map((expense: Expense, key: number) => (
            <Fragment>
              <ListItem
                key={key}
                containerStyle={
                  osTheme !== 'light' && styles.listContainerDarkMode
                }
                style={styles.listItem}>
                <ListItem.Content>
                  <ListItem.Title
                    style={osTheme !== 'light' && styles.listTextDarkMode}>
                    {expense.name}
                  </ListItem.Title>
                  <ListItem.Subtitle>
                    {'Value: ' + expense.price + ', from date: ' + expense.date}
                  </ListItem.Subtitle>
                </ListItem.Content>
              </ListItem>
            </Fragment>
          ))}
        </ScrollView>
      )}
    </Fragment>
  );
};

export default ListTransactionsTasks;
