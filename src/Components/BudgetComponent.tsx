import React, {
  Dispatch,
  Fragment,
  SetStateAction,
  useEffect,
  useState,
} from 'react';
import {ScrollView, StyleSheet, View} from 'react-native';
import {Badge, Button, ListItem, Tab} from 'react-native-elements';
import {Surface, Subheading, Paragraph, Text} from 'react-native-paper';
import Budget from '../Interfaces/Budget.interface';
import {Dimensions} from 'react-native';
import LinearProgress from 'react-native-elements/dist/linearProgress/LinearProgress';
import ListTransactionsTasks from './BudgetPage/ListTransactionsTasks';
import {BudgetListEnum} from '../Enums/BudgetListEnum';
import Task from '../Interfaces/Task.interface';
import Expense from '../Interfaces/Expense.interface';

interface Props {
  isAddExpenseOpened: boolean;
  setIsAddExpenseOpened: Dispatch<SetStateAction<boolean>>;
  setIsSelectTransactionPeriod: Dispatch<SetStateAction<boolean>>;
  budget: Budget;
  selectedTransactionPeriod: string;
  tasks: Task[];
  osTheme: string;
  expenses: Expense[];
  setExpenses: Dispatch<SetStateAction<Expense[]>>;
}

const styles = StyleSheet.create({
  surface: {
    borderRadius: 15,
    width: 250,
    height: 80,
    backgroundColor: '#4d908e',
    padding: 10,
    elevation: 3,
  },
  font: {
    color: '#e4e6eb',
    textAlign: 'center',
  },
  listItem: {padding: 0},
});

const BudgetComponent = ({
  isAddExpenseOpened,
  setIsAddExpenseOpened,
  setIsSelectTransactionPeriod,
  budget,
  selectedTransactionPeriod,
  tasks,
  osTheme,
  expenses,
  setExpenses,
}: Props) => {
  const [totalValueString, setTotalValueString] =
    React.useState<string>('Saved this month');
  const [percentageExpense, setPercentageExpense] = React.useState<number>(0);
  const [percentageIncome, setPercentageIncome] = React.useState<number>(0);
  const [listSelectedValue, setListSelectedValue] = useState<BudgetListEnum>(
    BudgetListEnum.tasks,
  );

  useEffect(() => {
    if (Math.sign(budget?.totalBalance) === 1) {
      setTotalValueString('Saved');
    } else if (Math.sign(budget?.totalBalance) === 0) {
      setTotalValueString('Nothing Saved');
    } else {
      setTotalValueString('Removed from savings.');
    }
    setPercentageExpense((budget.totalExpense / budget.expectedExpense) * 100);
    setPercentageIncome((budget.totalIncome / budget.expectedIncome) * 100);
  }, [
    budget.totalExpense,
    budget.expectedExpense,
    budget.totalIncome,
    budget.expectedIncome,
    budget.totalBalance,
  ]);

  const handleSelectListDisplay = (selected: BudgetListEnum) => {
    setListSelectedValue(selected);
  };

  return (
    <Fragment>
      <View
        style={{
          marginTop: 10,
          flexDirection: 'row',
          alignSelf: 'stretch',
          width: Dimensions.get('window').width,
          justifyContent: 'center',
        }}>
        <View>
          <Button
            type="outline"
            onPress={() => setIsAddExpenseOpened(true)}
            title="Add Expense"
            titleStyle={{color: '#f9c74f'}}
            buttonStyle={{
              width: Dimensions.get('window').width / 2.2,
              borderColor: '#f9c74f',
              alignSelf: 'stretch',
            }}
          />
        </View>
        <View>
          <Button
            type="outline"
            //onPress={() => setIsAddExpenseOpened(true)}
            title="Add Icome"
            titleStyle={{color: '#43aa8b'}}
            buttonStyle={{
              width: Dimensions.get('window').width / 2.2,
              borderColor: '#43aa8b',
              marginLeft: 15,
              alignSelf: 'stretch',
            }}
          />
        </View>
      </View>
      <View
        style={{justifyContent: 'center', alignItems: 'center', marginTop: 10}}>
        <Surface style={styles.surface}>
          <Subheading style={styles.font}>{budget.totalBalance}</Subheading>
          <Subheading style={styles.font}>{totalValueString}</Subheading>
        </Surface>
      </View>
      <View style={{flexDirection: 'row', marginTop: 10}}>
        <View>
          <Button
            type="clear"
            onPress={() => handleSelectListDisplay(BudgetListEnum.transactions)}
            title="Transactions"
            titleStyle={{color: '#43aa8b'}}
            buttonStyle={{
              width: Dimensions.get('window').width / 2,
              borderColor: '#43aa8b',
              alignSelf: 'stretch',
            }}
          />
        </View>
        <View>
          <Button
            type="clear"
            onPress={() => handleSelectListDisplay(BudgetListEnum.tasks)}
            title="Tasks"
            titleStyle={{color: '#43aa8b'}}
            buttonStyle={{
              width: Dimensions.get('window').width / 2,
              borderColor: '#43aa8b',
              alignSelf: 'stretch',
            }}
          />
        </View>
      </View>

      <ListTransactionsTasks
        isAddExpenseOpened={isAddExpenseOpened}
        selectedList={listSelectedValue}
        tasks={tasks}
        osTheme={osTheme}
        expenses={expenses}
        setExpenses={setExpenses}
      />
    </Fragment>
  );
};

export default BudgetComponent;