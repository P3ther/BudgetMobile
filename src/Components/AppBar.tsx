import React, {Fragment} from 'react';
import {Header} from 'react-native-elements';

interface Props {}
const AppBar = ({}: Props) => {
  return (
    <Fragment>
      <Header
        containerStyle={{backgroundColor: '#4d908e'}}
        leftContainerStyle={{marginTop: 5}}
        rightContainerStyle={{marginTop: 5}}
        centerContainerStyle={{marginTop: 14}}
        leftComponent={{
          icon: 'menu',
          color: '#000',
          iconStyle: {color: '#fff'},
        }}
        centerComponent={{text: 'Budget App', style: {color: '#fff'}}}
        rightComponent={{icon: 'home', color: '#fff'}}
      />
    </Fragment>
  );
};

export default AppBar;
