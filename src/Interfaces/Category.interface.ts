import Expense from './Expense.interface';
//import Income from './Income.interface';

export default interface Category {
  id?: number;
  name: string;
  limit: number;
  value?: number;
  type: number;
  visible: boolean | number;
  expenseList?: Expense[];
  incomeList?: Expense[];
  currencyId?: number;
  currencySign?: string;
}
