export default interface StartEndTime {
  startDateTime: Date;
  endDateTime: Date;
}
