import Expense from './Expense.interface'; //mport Income from './Income.interface';

export default interface Budget {
  id: number;
  expensesList: Expense[];
  incomeList: Expense[];
  totalBalance: number;
  totalExpense: number;
  totalIncome: number;
  expectedIncome: number;
  expectedExpense: number;
}
