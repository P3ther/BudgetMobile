import AsyncStorage from '@react-native-async-storage/async-storage';
import Budget from '../Interfaces/Budget.interface';
import {budgetLocalName} from './localStorageNames';

export const setBudgetLocal = (budget: Budget) => {
  AsyncStorage.setItem(budgetLocalName, JSON.stringify(budget));
};

export const getBudgetLocal = async (): Promise<Budget> => {
  let stringBudget = await AsyncStorage.getItem(budgetLocalName);
  let budget: Budget = {} as Budget;
  if (stringBudget !== null) {
    budget = JSON.parse(stringBudget);
  }
  return budget;
};
