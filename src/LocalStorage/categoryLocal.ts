import AsyncStorage from '@react-native-async-storage/async-storage';
import Category from '../Interfaces/Category.interface';
import {expenseCategoryLocalName} from './localStorageNames';

export const setExpenseCategoryLocal = (category: Category[]) => {
  AsyncStorage.setItem(expenseCategoryLocalName, JSON.stringify(category));
};

export const getExpenseCategoryLocal = async (): Promise<Category[]> => {
  let stringCategory = await AsyncStorage.getItem(expenseCategoryLocalName);
  let category: Category[] = [];
  if (stringCategory !== null) {
    category = JSON.parse(stringCategory);
  }
  return category;
};
