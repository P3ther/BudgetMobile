import AsyncStorage from '@react-native-async-storage/async-storage';
import Task from '../Interfaces/Task.interface';
import {taskLocalName} from './localStorageNames';

export const setTaskLocal = (tasks: Task[]) => {
  AsyncStorage.setItem(taskLocalName, JSON.stringify(tasks));
};

export const getTaskLocal = async (): Promise<Task[]> => {
  let stringTasks = await AsyncStorage.getItem(taskLocalName);
  let tasks: Task[] = [];
  if (stringTasks !== null) {
    tasks = JSON.parse(stringTasks);
  }
  return tasks;
};
