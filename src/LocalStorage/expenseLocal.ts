import AsyncStorage from '@react-native-async-storage/async-storage';
import Expense from '../Interfaces/Expense.interface';
import {expenseLocalName} from './localStorageNames';

export const setExpensesLocal = async (expenses: Expense[]) => {
  AsyncStorage.setItem(expenseLocalName, JSON.stringify(expenses));
};

export const getExpensesLocal = async (): Promise<Expense[]> => {
  let stringExpense = await AsyncStorage.getItem(expenseLocalName);
  let expenses: Expense[] = [];
  if (stringExpense !== null) {
    expenses = JSON.parse(stringExpense);
  }
  return expenses;
};
