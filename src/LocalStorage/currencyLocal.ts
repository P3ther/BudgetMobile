import AsyncStorage from '@react-native-async-storage/async-storage';
import Currency from '../Interfaces/Currency.interface';
import {currencyLocalName} from './localStorageNames';

export const setCurrencyLocal = (currency: Currency[]) => {
  AsyncStorage.setItem(currencyLocalName, JSON.stringify(currency));
};

export const getCurrencyLocal = async (): Promise<Currency[]> => {
  let stringCurrency = await AsyncStorage.getItem(currencyLocalName);
  let currency: Currency[] = [];
  if (stringCurrency !== null) {
    currency = JSON.parse(stringCurrency);
  }
  return currency;
};
