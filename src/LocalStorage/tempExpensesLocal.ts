import AsyncStorage from '@react-native-async-storage/async-storage';
import Expense from '../Interfaces/Expense.interface';
import {tempExpenseLocalName} from './localStorageNames';

export const setTempExpenseLocal = async (expense: Expense) => {
  let stringExpenses = await AsyncStorage.getItem(tempExpenseLocalName);
  let expenses: Expense[] = [];
  if (stringExpenses !== null) {
    expenses = JSON.parse(stringExpenses);
  }
  expenses.push(expense);
  AsyncStorage.setItem(tempExpenseLocalName, JSON.stringify(expenses));
};

export const getTempExpenseLocal = async (): Promise<Expense[]> => {
  let stringExpense = await AsyncStorage.getItem(tempExpenseLocalName);
  let expenses: Expense[] = [];
  if (stringExpense !== null) {
    expenses = JSON.parse(stringExpense);
  }
  return expenses;
};

export const removeTempExpenseLocal = async () => {
  AsyncStorage.removeItem(tempExpenseLocalName);
};
