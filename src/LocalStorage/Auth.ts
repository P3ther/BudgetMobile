import AsyncStorage from '@react-native-async-storage/async-storage';
import jwt from 'jwt-decode';

interface Token {
  sub: string;
  exp: number;
}

export const getToken = async () => {
  let result: string | null = await AsyncStorage.getItem('token');
  if (result !== null) {
    return result;
  } else {
    return '';
  }
};

export const isAuthenticated = async (): Promise<boolean> => {
  let tempToken: string | null = await AsyncStorage.getItem('token');
  let tokenUnd: string | null = tempToken!.substr(7);
  let token: Token | null = null;
  if (tokenUnd !== null) {
    token = jwt(tokenUnd);
  }
  let tokenExp: number | undefined = token?.exp;
  let date: number = Date.now() / 1000;
  if (tokenExp! > date && token !== null) {
    return true;
  } else {
    removeUserSession();
    return false;
  }
};

export const setUserSession = (token: string, user: string) => {
  AsyncStorage.setItem('token', token);
  AsyncStorage.setItem('user', JSON.stringify(user));
};

export const removeUserSession = async () => {
  await AsyncStorage.removeItem('token');
  await AsyncStorage.removeItem('user');
};
