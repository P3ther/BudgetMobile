export const budgetLocalName = 'budgetLocal';

export const expenseCategoryLocalName = 'expenseCategoryLocal';

export const currencyLocalName = 'currencyLocal';

export const expenseLocalName = 'expenseLocal';

export const tempExpenseLocalName = 'tempExpenseLocal';

export const taskLocalName = 'taskLocal';
