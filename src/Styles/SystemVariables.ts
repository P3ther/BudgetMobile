export const systemDarkThemeBackground: string = '#2e2e2e';
export const systemLightThemeBackground: string = '#fafafa';

export const systemDarkThemeFont: string = '#e1e1e1';
