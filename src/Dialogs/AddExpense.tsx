import React, {
  Dispatch,
  Fragment,
  SetStateAction,
  useEffect,
  useState,
} from 'react';
import {
  Keyboard,
  PlatformColor,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  useColorScheme,
  View,
} from 'react-native';
import {
  Button,
  Paragraph,
  Dialog,
  Portal,
  TextInput,
  Menu,
} from 'react-native-paper';
import {Controller, useForm} from 'react-hook-form';
import Expense from '../Interfaces/Expense.interface';
import DateTimePicker from '@react-native-community/datetimepicker';
import {getToken} from '../LocalStorage/Auth';
import Category from '../Interfaces/Category.interface';
import Currency from '../Interfaces/Currency.interface';
import moment from 'moment';
import {expenseApi} from '../Api/expenseApi';
import NetInfo, {NetInfoState} from '@react-native-community/netinfo';
import {setTempExpenseLocal} from '../LocalStorage/tempExpensesLocal';
import {BottomSheet, ListItem} from 'react-native-elements';

interface Props {
  isOpened: boolean;
  setIsOpened: Dispatch<SetStateAction<boolean>>;
  expenseCategories: Category[];
  currencies: Currency[];
}

type AndroidMode = 'date' | 'time';

const AddExpense = ({
  isOpened,
  setIsOpened,
  expenseCategories,
  currencies,
}: Props) => {
  const [selectCategory, setSelectCategory] = useState<boolean>(false);
  const [selectedCategory, setSelectedCategory] = useState<string>('');
  const [selectedCurrency, setSelectedCurrency] = useState<string>('');
  const [showDateTimePicker, setShowDateTimePicker] = useState<boolean>(false);
  const [selectCurrency, setSelectCurrency] = useState<boolean>(false);
  const [dateTimePickerMode, setDateTimePickerMode] = useState<
    AndroidMode | undefined
  >('date');
  const [selectedDateTime, setSelectedDateTime] = useState<Date>(new Date());
  const {
    handleSubmit,
    control,
    formState: {isDirty, isValid},
    getValues,
    reset,
  } = useForm<Expense>({
    //mode: 'onChange',
    //resolver: yupResolver(AddExpenseValidation),
  });

  useEffect(() => {
    // Handle displaying Name of cattegory in text field, because we are selecting ID.
    let resultString = '';
    let result: Category[] = expenseCategories.filter(
      expenseCategory => expenseCategory.id === getValues().category,
    );
    if (result[0] !== undefined) {
      resultString = result[0].name;
    }
    setSelectedCategory(resultString);
  }, [getValues().category]);

  useEffect(() => {
    let resultString = '';
    let result: Currency[] = currencies.filter(
      currency => currency.id === getValues().currencyId,
    );
    if (result[0] !== undefined) {
      resultString = result[0].name + ' (' + result[0].sign + ')';
    }
    setSelectedCurrency(resultString);
  }, [getValues().currencyId]);

  const onSubmit = async (data: Expense) => {
    let newExpense: Expense = {
      id: 0,
      name: data['name'],
      date: selectedDateTime,
      price: data['price'],
      addressToReceipt: '',
      category: data['category'],
      categoryName: '',
      currencyId: data['currencyId'],
      currencySign: '',
    };
    NetInfo.fetch().then((isConnected: NetInfoState) => {
      if (isConnected.isConnected) {
        submitExpenseOnline(newExpense).then(res => handleClose());
      } else {
        setTempExpenseLocal(newExpense).then(() => handleClose());
      }
    });
  };

  const handleClose = () => {
    reset();
    setIsOpened(false);
  };

  const submitExpenseOnline = async (expense: Expense) => {
    await expenseApi
      .addExpense(expense, await getToken())
      .then(res => console.log(res))
      .catch(err => console.log(err));
  };

  const handleChangeDateTimePicker = (
    event: Event,
    selectedDate: Date | undefined,
  ) => {
    if (selectedDate !== undefined) {
      setSelectedDateTime(new Date(selectedDate));
    }
    if (dateTimePickerMode === 'date') {
      setDateTimePickerMode('time');
    } else {
      setShowDateTimePicker(false);
      setDateTimePickerMode('date');
    }
  };

  return (
    <Portal>
      <Dialog visible={isOpened} onDismiss={handleClose}>
        <Dialog.Title>Add new Expense</Dialog.Title>
        <Dialog.Content>
          <Paragraph>Name of the Expense</Paragraph>
          <Controller
            control={control}
            name="name"
            render={({field: {onChange, value, onBlur, ref}}) => (
              <TextInput
                dense
                value={value}
                onChange={value => onChange(value.nativeEvent.text)}
                ref={ref}
              />
            )}
          />
          <Paragraph>Category</Paragraph>
          <Controller
            control={control}
            name="category"
            render={({field: {onChange, value, onBlur, ref}}) => (
              <Fragment>
                <TextInput
                  dense
                  onChange={value => {
                    setSelectCategory(false);
                    onChange(value.nativeEvent.text);
                  }}
                  onFocus={() => {
                    setShowDateTimePicker(false);
                    setSelectCategory(true);
                  }}
                  ref={ref}
                  showSoftInputOnFocus={false}
                  value={selectedCategory}
                />
                <BottomSheet
                  modalProps={{
                    visible: selectCategory,
                  }}
                  containerStyle={{backgroundColor: 'rgba(0.5, 0.25, 0, 0.4)'}}>
                  <ListItem
                    disabled
                    containerStyle={{backgroundColor: '#383838'}}
                    bottomDivider>
                    <ListItem.Content>
                      <ListItem.Title style={{color: '#c5c5c5'}}>
                        Select Category
                      </ListItem.Title>
                    </ListItem.Content>
                  </ListItem>
                  {expenseCategories?.map((category: Category) => (
                    <ListItem
                      key={category.id}
                      onPress={() => {
                        onChange(category.id);
                        setSelectCategory(false);
                      }}
                      containerStyle={{backgroundColor: '#383838'}}>
                      <ListItem.Content>
                        <ListItem.Title style={{color: '#ededed'}}>
                          {category.name}
                        </ListItem.Title>
                      </ListItem.Content>
                    </ListItem>
                  ))}
                </BottomSheet>
              </Fragment>
            )}
          />
          <Paragraph>Price of the Expense</Paragraph>
          <Controller
            control={control}
            name="price"
            render={({field: {onChange, value, onBlur, ref}}) => (
              <TextInput
                dense
                value={value?.toString()}
                onChange={value => onChange(value.nativeEvent.text)}
                ref={ref}
                keyboardType="number-pad"
              />
            )}
          />
          <Paragraph>Date of the Expense</Paragraph>
          <Controller
            control={control}
            name="date"
            render={({field: {onChange, value, onBlur, ref}}) => (
              <TextInput
                dense
                value={moment(selectedDateTime)
                  .format('DD MM YYYY, HH:mm')
                  .toString()}
                onChange={value => onChange(value.nativeEvent.text)}
                ref={ref}
                disabled
              />
            )}
          />
          <Button onPress={() => setShowDateTimePicker(true)}>
            Select Date
          </Button>
          {showDateTimePicker && (
            <DateTimePicker
              value={selectedDateTime}
              onChange={handleChangeDateTimePicker}
              mode={dateTimePickerMode}
            />
          )}
          <Paragraph>Currency of the Expense</Paragraph>
          <Controller
            control={control}
            name="currencyId"
            render={({field: {onChange, value, onBlur, ref}}) => (
              <Fragment>
                <TextInput
                  dense
                  onChange={value => onChange(value.nativeEvent.text)}
                  onFocus={() => setSelectCurrency(true)}
                  ref={ref}
                  showSoftInputOnFocus={false}
                  value={selectedCurrency}
                />
                <BottomSheet
                  modalProps={{
                    visible: selectCurrency,
                  }}
                  containerStyle={{backgroundColor: 'rgba(0.5, 0.25, 0, 0.4)'}}>
                  <ListItem
                    disabled
                    containerStyle={{backgroundColor: '#383838'}}
                    bottomDivider>
                    <ListItem.Content>
                      <ListItem.Title style={{color: '#c5c5c5'}}>
                        Select Currency
                      </ListItem.Title>
                    </ListItem.Content>
                  </ListItem>
                  {currencies?.map((currency: Currency) => (
                    <ListItem
                      key={currency.id}
                      onPress={() => {
                        onChange(currency.id);
                        setSelectCurrency(false);
                      }}
                      containerStyle={{backgroundColor: '#383838'}}>
                      <ListItem.Content>
                        <ListItem.Title style={{color: '#ededed'}}>
                          {currency.name + ' (' + currency.sign + ')'}
                        </ListItem.Title>
                      </ListItem.Content>
                    </ListItem>
                  ))}
                </BottomSheet>
              </Fragment>
            )}
          />
        </Dialog.Content>
        <Dialog.Actions>
          <Button onPress={handleClose} color="#f50057">
            Close
          </Button>
          <Button
            onPress={handleSubmit(onSubmit)}
            mode="contained"
            style={{backgroundColor: '#43aa8b'}}>
            Submit
          </Button>
        </Dialog.Actions>
      </Dialog>
    </Portal>
  );
};

export default AddExpense;
